{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# Native data structures\n",
    "One of the things that makes python great to work with for data intensive applications is its native support of data structures."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "There are two major *workhorse* data structures in python: **lists** (or arrays) and **dictionaries**."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "All popular data-science packages are heavily inspired by the notations and the concepts that you will encounter in this module."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# Lists\n",
    "A list is an ordered collection of elements of any data type supported by python. These collections are also *iterable* which means that we can use them in our **`for`** loops. Lists are interesting because they allow us to put many things together that belong together and we can perform operations in them such as sorting, filtering, etc."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "In python you declare a list by using the *square bracket notation*, like this:\n",
    "```python\n",
    "\n",
    "<identifier> = []\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 14,
   "metadata": {
    "collapsed": true,
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [],
   "source": [
    "vegetables = [\"carrot\", \"potato\", \"onion\", \"leek\", \"aubergine\"]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "### Accessing members of a list\n",
    "\n",
    "```python\n",
    "vegetables = [\"carrot\", \"potato\", \"onion\", \"leek\", \"aubergine\"]\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [
    {
     "data": {
      "text/plain": [
       "'carrot'"
      ]
     },
     "execution_count": 3,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "# using index notation\n",
    "vegetables[0]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [
    {
     "data": {
      "text/plain": [
       "'leek'"
      ]
     },
     "execution_count": 5,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "# indexes begin counting from 0\n",
    "# so accessing element with index 3, will give us the fourth vegetable\n",
    "vegetables[3]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "carrot\n",
      "potato\n",
      "onion\n",
      "leek\n",
      "aubergine\n"
     ]
    }
   ],
   "source": [
    "# using a for loop to iterate through the members of a list\n",
    "for veggy in vegetables:\n",
    "    print(veggy)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 135,
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [
    {
     "data": {
      "text/plain": [
       "['aubergine', 'leek', 'onion', 'potato', 'carrot']"
      ]
     },
     "execution_count": 135,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "vegetables = [\"carrot\", \"potato\", \"onion\", \"leek\", \"aubergine\"]\n",
    "# the following call reverses the order of the elements in our list\n",
    "vegetables.reverse()\n",
    "vegetables"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "### Adding and removing elements"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 150,
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [
    {
     "data": {
      "text/plain": [
       "['carrot', 'potato', 'celery']"
      ]
     },
     "execution_count": 150,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "vegetables = [\"carrot\", \"potato\"]\n",
    "# we can use append to add an element to the end of our list\n",
    "vegetables.append(\"celery\")\n",
    "vegetables"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 151,
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [
    {
     "data": {
      "text/plain": [
       "['carrot', 'kale', 'potato', 'celery']"
      ]
     },
     "execution_count": 151,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "# we can also insert an element in a specific position in our list\n",
    "# let's add \"kale\" between \"carrot and \"potato\"\n",
    "vegetables.insert(1, \"kale\") # we insert it in position 1 (or second)\n",
    "vegetables"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 161,
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [
    {
     "data": {
      "text/plain": [
       "['carrot', 'potato', 'apple', 'grape']"
      ]
     },
     "execution_count": 161,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "# if we add two lists together, the resulting list will contain all the elements from both\n",
    "vegetables = [\"carrot\", \"potato\"]\n",
    "fruits = [\"apple\", \"grape\"]\n",
    "smoothie = vegetables + fruits\n",
    "smoothie"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 162,
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [
    {
     "data": {
      "text/plain": [
       "['carrot', 'apple', 'grape']"
      ]
     },
     "execution_count": 162,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "# we can remove items in a list by calling the remove method\n",
    "smoothie.remove(\"potato\")\n",
    "smoothie"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## Nesting"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 163,
   "metadata": {
    "collapsed": true,
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [],
   "source": [
    "# lists can nest, we can have one list inside of another list\n",
    "vegetables = [[\"carrot\", \"potato\"], \"lettuce\", \"artichoke\"]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 164,
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [
    {
     "data": {
      "text/plain": [
       "['carrot', 'potato']"
      ]
     },
     "execution_count": 164,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "# in this case our first element is in itself a list\n",
    "vegetables[0]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 165,
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [
    {
     "data": {
      "text/plain": [
       "'lettuce'"
      ]
     },
     "execution_count": 165,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "# while our second is still a single string\n",
    "vegetables[1]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 166,
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [
    {
     "data": {
      "text/plain": [
       "'potato'"
      ]
     },
     "execution_count": 166,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "# the first index returns the list in element 0 \n",
    "# and the second index retrieves the second element of our nested list\n",
    "vegetables[0][1]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 168,
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [
    {
     "data": {
      "text/plain": [
       "2"
      ]
     },
     "execution_count": 168,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "# to find the position of an element in a list we can call\n",
    "vegetables.index(\"artichoke\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "### Data types in a list"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 28,
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "<class 'int'>\n",
      "<class 'float'>\n",
      "<class 'float'>\n",
      "<class 'str'>\n",
      "<class 'list'>\n",
      "<class 'str'>\n"
     ]
    }
   ],
   "source": [
    "# a list can hold items of hetereogeneous data types\n",
    "lostandfound = [12, 3.0, 0.00000002, \"banana\", [\"fear\", \"loathing\"], \"Washington\"]\n",
    "for item in lostandfound:\n",
    "    print(type(item))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [],
   "source": [
    "# deeply nested lists\n",
    "lostandfound = [['1.1', '1.2'], ['2.1', '2.2'], [['3.1.1', '3.1.2'], ['3.2.1', '3.2.2'], ['3.3.1', ['3.3.1.1', '3.3.2.1']]]]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 47,
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [
    {
     "data": {
      "text/plain": [
       "'3.3.1.1'"
      ]
     },
     "execution_count": 47,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "lostandfound[2][2][1][0]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## Slicing\n",
    "Slicing is a way of getting *views* of lists to for example, obtain subsets, inversions, etc. This is an important functionality in python and one that is used in scientific packages like *numpy* and *Pandas* **all the time**. To create a slice we use the slice operator, denoted by a colon character `:`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 48,
   "metadata": {
    "collapsed": true,
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [],
   "source": [
    "# let's start with a human readable list\n",
    "lst = [\"one\", \"two\", \"three\", \"four\", \"five\", \"six\", \"seven\", \"eight\", \"nine\", \"ten\"]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 50,
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [
    {
     "data": {
      "text/plain": [
       "['one', 'two']"
      ]
     },
     "execution_count": 50,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "# we create a slice starting at element 0 until (but not including) element 2\n",
    "lst[0:2]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 52,
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [
    {
     "data": {
      "text/plain": [
       "['three', 'four', 'five', 'six', 'seven']"
      ]
     },
     "execution_count": 52,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "# create a slice form element 2 (inclusive) to element 7 (not inclusive)\n",
    "lst[2:7]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## Slicing (cont.)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 53,
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [
    {
     "data": {
      "text/plain": [
       "['one', 'two', 'three', 'four', 'five', 'six', 'seven']"
      ]
     },
     "execution_count": 53,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "# if we omit the starting element of our slice, our slice will start from the first element\n",
    "lst[:7]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 55,
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [
    {
     "data": {
      "text/plain": [
       "['seven', 'eight', 'nine', 'ten']"
      ]
     },
     "execution_count": 55,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "# if we omit the ending element, our slice will end in the last element\n",
    "lst[6:]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 59,
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [
    {
     "data": {
      "text/plain": [
       "['one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight']"
      ]
     },
     "execution_count": 59,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "# the slice operator can take negative indexes\n",
    "# you can read this as take all elements except the last two\n",
    "lst[:-2]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 60,
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [
    {
     "data": {
      "text/plain": [
       "['nine', 'ten']"
      ]
     },
     "execution_count": 60,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "# or include only the last two elements in our slice\n",
    "lst[-2:]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## Slicing (cont.)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 169,
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [
    {
     "data": {
      "text/plain": [
       "['one', 'three', 'five']"
      ]
     },
     "execution_count": 169,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "# the slicing operator can take a third number, which indicates the number of elements\n",
    "# to skip in our slice (or step-over)\n",
    "lst[0:6:2]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 170,
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [
    {
     "data": {
      "text/plain": [
       "['one', 'three', 'five', 'seven', 'nine']"
      ]
     },
     "execution_count": 170,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "# in this slice we take \"every other\" element of our list\n",
    "lst[::2]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## The *in* operator\n",
    "Python has an *in* operator that allows us to check if an item is contained in a list. The operator returns `True` if the element is found in the list and `False` if it isn't."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true,
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [],
   "source": [
    "# let's start with a human readable list\n",
    "lst = [\"one\", \"two\", \"three\", \"four\", \"five\", \"six\", \"seven\", \"eight\", \"nine\", \"ten\"]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 64,
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [
    {
     "data": {
      "text/plain": [
       "True"
      ]
     },
     "execution_count": 64,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "\"four\" in lst"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 172,
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [
    {
     "data": {
      "text/plain": [
       "False"
      ]
     },
     "execution_count": 172,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "\"four\" not in lst"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 173,
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [
    {
     "data": {
      "text/plain": [
       "True"
      ]
     },
     "execution_count": 173,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "# you can think of a string as a list of characters\n",
    "\"y\" in \"Python\""
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# Dictionaries\n",
    "Another first-class citizen kind of data structure is the *dictionary*. A dictionary is simply a special kind of list that contains *key-value pairs* and is not ordered like a list. To access elements we do not use the index, but rather used the a *key* to get it's corresponding *value*."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "To declare a dictionary in python we use the curly-bracket notation, like this:\n",
    "```python\n",
    "    <identifier> = { <key1> : <value1>, <key2> : <value2>, ... }\n",
    "```\n",
    "\n",
    "Keys and values can be any data type supported by python."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## Dictionaries (cont.)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 174,
   "metadata": {
    "collapsed": true,
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [],
   "source": [
    "population = {\"Argentina\": 41343200, \"Chile\": 16746490, \"Ecuador\": 14790610, \"Colombia\": 44205290, \"Brazil\": 201103330}"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 175,
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [
    {
     "data": {
      "text/plain": [
       "16746490"
      ]
     },
     "execution_count": 175,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "# to access a value we can use its key\n",
    "population['Chile']"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 176,
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [
    {
     "data": {
      "text/plain": [
       "58089690"
      ]
     },
     "execution_count": 176,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "population['Chile'] + population['Argentina']"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [
    {
     "ename": "NameError",
     "evalue": "name 'population' is not defined",
     "output_type": "error",
     "traceback": [
      "\u001b[0;31m---------------------------------------------------------------------------\u001b[0m",
      "\u001b[0;31mNameError\u001b[0m                                 Traceback (most recent call last)",
      "\u001b[0;32m<ipython-input-3-6bdf2abd24c4>\u001b[0m in \u001b[0;36m<module>\u001b[0;34m()\u001b[0m\n\u001b[1;32m      1\u001b[0m \u001b[0;31m# if we try to access an element that doesn't exist we will get a KeyError\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[0;32m----> 2\u001b[0;31m \u001b[0mpopulation\u001b[0m\u001b[0;34m[\u001b[0m\u001b[0;34m\"Netherlands\"\u001b[0m\u001b[0;34m]\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[0m",
      "\u001b[0;31mNameError\u001b[0m: name 'population' is not defined"
     ]
    }
   ],
   "source": [
    "# if we try to access an element that doesn't exist we will get a KeyError\n",
    "population[\"Netherlands\"]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [
    {
     "ename": "NameError",
     "evalue": "name 'population' is not defined",
     "output_type": "error",
     "traceback": [
      "\u001b[0;31m---------------------------------------------------------------------------\u001b[0m",
      "\u001b[0;31mNameError\u001b[0m                                 Traceback (most recent call last)",
      "\u001b[0;32m<ipython-input-4-0cc111de2235>\u001b[0m in \u001b[0;36m<module>\u001b[0;34m()\u001b[0m\n\u001b[1;32m      1\u001b[0m \u001b[0;31m# with dictionaries we are expected to use keys, not cardinal indexes\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[0;32m----> 2\u001b[0;31m \u001b[0mpopulation\u001b[0m\u001b[0;34m[\u001b[0m\u001b[0;36m0\u001b[0m\u001b[0;34m]\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[0m",
      "\u001b[0;31mNameError\u001b[0m: name 'population' is not defined"
     ]
    }
   ],
   "source": [
    "# with dictionaries we are expected to use keys, not cardinal indexes\n",
    "population[0]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## Growing a dictionary"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 179,
   "metadata": {
    "collapsed": true,
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [],
   "source": [
    "# you can start with an empty dictionary like this:\n",
    "patient = {}"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 180,
   "metadata": {
    "scrolled": false,
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [
    {
     "data": {
      "text/plain": [
       "{'age': 46,\n",
       " 'height': 171,\n",
       " 'id': 123456789,\n",
       " 'name': 'Satoru',\n",
       " 'surname': 'Takeshi',\n",
       " 'weight': 77}"
      ]
     },
     "execution_count": 180,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "# and add keys and values as you need them\n",
    "patient['name'] = \"Satoru\"\n",
    "patient['surname'] = \"Takeshi\"\n",
    "patient['id'] = 123456789\n",
    "patient['age'] = 46\n",
    "patient['weight'] = 77\n",
    "patient['height'] = 171\n",
    "# the result will be a dictionary containing all the k-v pairs\n",
    "patient"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 181,
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [
    {
     "data": {
      "text/plain": [
       "'Takeshi'"
      ]
     },
     "execution_count": 181,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "patient['surname']"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 182,
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [
    {
     "ename": "KeyError",
     "evalue": "'nationality'",
     "output_type": "error",
     "traceback": [
      "\u001b[0;31m---------------------------------------------------------------------------\u001b[0m",
      "\u001b[0;31mKeyError\u001b[0m                                  Traceback (most recent call last)",
      "\u001b[0;32m<ipython-input-182-5f8fffff5eda>\u001b[0m in \u001b[0;36m<module>\u001b[0;34m()\u001b[0m\n\u001b[0;32m----> 1\u001b[0;31m \u001b[0mpatient\u001b[0m\u001b[0;34m[\u001b[0m\u001b[0;34m'nationality'\u001b[0m\u001b[0;34m]\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[0m",
      "\u001b[0;31mKeyError\u001b[0m: 'nationality'"
     ]
    }
   ],
   "source": [
    "patient['nationality']"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 183,
   "metadata": {
    "collapsed": true,
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [],
   "source": [
    "patient['nationality'] = \"Japanese\""
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "### Operators\n",
    "\n",
    "| Operator | Explanation |\n",
    "|:---|:---|\n",
    "| **len(d)** | returns the number of stored entries, i.e. the number of (key,value) pairs. |\n",
    "| **del d[k]** | deletes the key k together with his value |\n",
    "| **k in d** | True, if a key k exists in the dictionary d |\n",
    "| **k not in d** | True, if a key k doesn't exist in the dictionary d |\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 190,
   "metadata": {
    "collapsed": true,
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "population = {\"Argentina\": 41343200, \"Chile\": 16746490, \"Ecuador\": 14790610, \"Colombia\": 44205290, \"Brazil\": 201103330}"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 191,
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [
    {
     "data": {
      "text/plain": [
       "5"
      ]
     },
     "execution_count": 191,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "# get length of our dictionary (or the number of key-value pairs)\n",
    "len(population)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 192,
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [
    {
     "data": {
      "text/plain": [
       "True"
      ]
     },
     "execution_count": 192,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "# is \"Colombia\" a key in our dictionary?\n",
    "\"Colombia\" in population"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 193,
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [
    {
     "data": {
      "text/plain": [
       "True"
      ]
     },
     "execution_count": 193,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "# is \"Netherlands\" not a key in our dictionary?\n",
    "\"Netherlands\" not in population"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "### Operators (deletion)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 83,
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [
    {
     "data": {
      "text/plain": [
       "{'Argentina': 41343200,\n",
       " 'Chile': 16746490,\n",
       " 'Colombia': 44205290,\n",
       " 'Ecuador': 14790610}"
      ]
     },
     "execution_count": 83,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "population = {\"Argentina\": 41343200, \"Chile\": 16746490, \"Ecuador\": 14790610, \"Colombia\": 44205290, \"Brazil\": 201103330}\n",
    "# remove element form our dictionary with key \"Brazil\"\n",
    "del population[\"Brazil\"]\n",
    "population"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## Iterating over dictionaries"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 189,
   "metadata": {
    "collapsed": true,
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [],
   "source": [
    "population = {\"Argentina\": 41343200, \"Chile\": 16746490, \"Ecuador\": 14790610, \"Colombia\": 44205290, \"Brazil\": 201103330}"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 186,
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [
    {
     "data": {
      "text/plain": [
       "dict_keys(['Argentina', 'Chile', 'Ecuador', 'Colombia', 'Brazil'])"
      ]
     },
     "execution_count": 186,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "# with the .keys() method call we get all the keys in a dictionary as a list\n",
    "population.keys()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 187,
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [
    {
     "data": {
      "text/plain": [
       "dict_values([41343200, 16746490, 14790610, 44205290, 201103330])"
      ]
     },
     "execution_count": 187,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "# with the .value() method we get all the values without keys\n",
    "population.values()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## Iterating over dictionaries (cont.)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 194,
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "41343200 live in Argentina\n",
      "16746490 live in Chile\n",
      "14790610 live in Ecuador\n",
      "44205290 live in Colombia\n",
      "201103330 live in Brazil\n"
     ]
    }
   ],
   "source": [
    "# we can iterate through the keys\n",
    "for country in population.keys():\n",
    "    print(f\"{population[country]} live in {country}\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 195,
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "41343200 live in Argentina\n",
      "16746490 live in Chile\n",
      "14790610 live in Ecuador\n",
      "44205290 live in Colombia\n",
      "201103330 live in Brazil\n"
     ]
    }
   ],
   "source": [
    "# we can get the key-value pair from the dict in a single statement\n",
    "for k, v in population.items():\n",
    "    print(f\"{v} live in {k}\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 196,
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Total population is 318188920\n"
     ]
    }
   ],
   "source": [
    "# we can get the values only and work with them\n",
    "# for example lets add all the populations together\n",
    "total = 0\n",
    "for v in population.values():\n",
    "    total += v\n",
    "print(f\"Total population is {total}\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## Merging dictionaries"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 100,
   "metadata": {
    "collapsed": true,
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [],
   "source": [
    "population_sa = {\"Argentina\": 41343200, \"Chile\": 16746490, \"Ecuador\": 14790610, \"Colombia\": 44205290, \"Brazil\": 201103330}\n",
    "population_eu = {\"Germany\": 82438639, \"France\": 65480710, \"Italy\": 59216525}"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 101,
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [
    {
     "data": {
      "text/plain": [
       "{'Argentina': 41343200,\n",
       " 'Brazil': 201103330,\n",
       " 'Chile': 16746490,\n",
       " 'Colombia': 44205290,\n",
       " 'Ecuador': 14790610,\n",
       " 'France': 65480710,\n",
       " 'Germany': 82438639,\n",
       " 'Italy': 59216525}"
      ]
     },
     "execution_count": 101,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "population = {}\n",
    "population.update(population_sa)\n",
    "population.update(population_eu)\n",
    "population"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## Making a dictionary from two lists"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 117,
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [
    {
     "data": {
      "text/plain": [
       "{'Finland': 'Helsinki',\n",
       " 'Germany': 'Berlin',\n",
       " 'Greece': 'Athens',\n",
       " 'Spain': 'Madrid',\n",
       " 'UK': 'London'}"
      ]
     },
     "execution_count": 117,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "countries = [\"Spain\", \"Germany\", \"Greece\", \"Finland\", \"UK\", \"Italy\"]\n",
    "capitals = [\"Madrid\", \"Berlin\", \"Athens\", \"Helsinki\", \"London\"]\n",
    "some_capitals = dict(zip(countries, capitals))\n",
    "some_capitals"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# Nesting dictionaries and lists\n",
    "One very powerful feature of python is it's capacity to seamlessly nest data structures. You can make a list of dictionaries, or have lists as values in your dictionary, etc. Making it very powerful."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 130,
   "metadata": {
    "collapsed": true,
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [],
   "source": [
    "# let's declare a list of patients\n",
    "patients = [\n",
    "    {\n",
    "        'surname': 'Takeshi',\n",
    "        'weight': 77,          # in kg\n",
    "        'height': 1.71         # in meters\n",
    "    },\n",
    "    {\n",
    "        'surname': 'Chitilova',\n",
    "        'weight': 55,\n",
    "        'height': 1.66\n",
    "    },\n",
    "    {\n",
    "        'surname': 'Galanis',\n",
    "        'weight': 90,\n",
    "        'height': 1.78\n",
    "    },\n",
    "    {\n",
    "        'surname': 'Arif',\n",
    "        'weight': 78,\n",
    "        'height': 1.83\n",
    "    },\n",
    "    {\n",
    "        'surname': 'Bulan',\n",
    "        'weight': 102,\n",
    "        'height': 1.93\n",
    "    },\n",
    "]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 131,
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [
    {
     "data": {
      "text/plain": [
       "{'height': 1.71, 'surname': 'Takeshi', 'weight': 77}"
      ]
     },
     "execution_count": 131,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "patients[0]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 132,
   "metadata": {
    "collapsed": true,
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [],
   "source": [
    "patients[0]['blood_sugar_levels'] = {\"week\" : \"12-03-2019\", \"samples\":[70, 76, 80, 80, 83, 78, 85]}"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 133,
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [
    {
     "data": {
      "text/plain": [
       "{'blood_sugar_levels': {'samples': [70, 76, 80, 80, 83, 78, 85],\n",
       "  'week': '12-03-2019'},\n",
       " 'height': 1.71,\n",
       " 'surname': 'Takeshi',\n",
       " 'weight': 77}"
      ]
     },
     "execution_count": 133,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "patients[0]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# Exercise\n",
    "Let's use the `bmi` library that we created earlier this morning to calculate the BMI of all the patients in the list above.\n",
    "\n",
    "```python\n",
    "patients = [\n",
    "    {\n",
    "        'surname': 'Takeshi',\n",
    "        'weight': 77,          # in kg\n",
    "        'height': 1.71         # in meters\n",
    "    },\n",
    "    {\n",
    "        'surname': 'Chitilova',\n",
    "        'weight': 55,\n",
    "        'height': 1.66\n",
    "    },\n",
    "    {\n",
    "        'surname': 'Galanis',\n",
    "        'weight': 90,\n",
    "        'height': 1.78\n",
    "    },\n",
    "    {\n",
    "        'surname': 'Arif',\n",
    "        'weight': 78,\n",
    "        'height': 1.83\n",
    "    },\n",
    "    {\n",
    "        'surname': 'Bulan',\n",
    "        'weight': 102,\n",
    "        'height': 1.93\n",
    "    },\n",
    "]\n",
    "```"
   ]
  }
 ],
 "metadata": {
  "celltoolbar": "Slideshow",
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.1"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
