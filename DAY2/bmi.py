
__doc__ = """The too-simplistic BMI calculator and report system, featuring over-simplified, biased and practically offensive math
Written at MOLMED@EMC during a Python for Data Science workshop"""

def calculate_bmi(height, weight):
    return weight/height**2

def report_bmi(bmi):
    if(bmi < 18.5):
        print("is underweight")
    elif(18.5 < bmi < 24.9):
        print("is norm")
    elif(25 < bmi < 29.9):
        print("is overweight")
    else:
        print("is obese")